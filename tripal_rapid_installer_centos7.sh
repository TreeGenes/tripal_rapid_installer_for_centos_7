#!/usr/bin/bash
# Author: Risharde Ramnath (www.risharde.com)
# Written and tested on 26th January 2022
# Support Tripal by visiting http://tripal.info/
#
#
rm -rf /var/www/html/*
rm -rf /var/lib/pgsql/
yum remove postgres* -y
echo "Welcome to Tripal Rapid Installer for CENTOS 7"
echo "----------------------------------------------"
echo ""
echo "* Make sure server has at least 2GB of RAM!"
echo ""
echo "[INFO] Setting up DRUPAL_HOME variable ..."
INSTALL_HOME=$(pwd)
echo "INSTALL HOME IS:"
echo $INSTALL_HOME
DRUPAL_HOME=/var/www/html/drupal
mkdir -p $DRUPAL_HOME
systemctl stop postgresql.service
systemctl stop httpd.service
echo "[INFO] Installing CENTOS Development Tools..."
echo "[INFO] You need to be connected to the internet, this takes a while..."
yum groupinstall "Development Tools" -y
echo "[INFO] Installing WGET and NANO ..."
yum install wget nano -y
echo "[INFO] Installing Apache HTTPD Server ..."
yum install httpd -y
echo "[INFO] Setting up HTTPD service settings ..."
systemctl start httpd.service
systemctl enable httpd.service
echo "[INFO] Copying httpd.conf to HTTPD settings folder ..."
yes | cp -rf httpd.conf /etc/httpd/conf/httpd.conf
echo "[INFO] Restarting HTTPD service ..."
systemctl restart httpd.service
echo "[INFO] Install EPEL Release"
yum install epel-release -y
echo "[INFO] Install REMI REPO to get PHP 7.2 packages"
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
yum install yum-utils -y
yum-config-manager --enable remi-php72
yum install php php-mcrypt php-cli php-gd php-curl php-xml php-mysql php-pgsql php-ldap php-zip php-fileinfo -y
php -v
yes | cp -rf php.ini /etc/php.ini
systemctl restart httpd.service
#yum install postgresql-server -y
#postgresql-setup initdb
yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm -y
yum install postgresql10 postgresql10-server postgresql10-contrib postgresql10-libs -y
/usr/pgsql-10/bin/postgresql-10-setup initdb
yes | cp -rf pg_hba.conf /var/lib/pgsql/10/data/pg_hba.conf
systemctl start postgresql-10.service
systemctl enable postgresql-10.service
chcon -R -t httpd_sys_content_rw_t /var/www/html
setsebool -P httpd_can_network_connect_db on
wget https://github.com/drush-ops/drush/releases/download/8.3.0/drush.phar
php drush.phar core-status
chmod +x drush.phar
yes | mv drush.phar /usr/bin/drush
drush init
echo "If you are asked for a role password, use 'drupal' without the quotes"
echo "Later on you'll need this to set up the database in which case you will use 'drupal' for all the values asked"
su postgres -c 'createuser -P drupal'
su postgres -c 'createdb drupal -O drupal'
WEB_HOME=/var/www/html
PHPPGADMIN_HOME=/var/www/html/phppgadmin
cd $WEB_HOME
wget https://github.com/phppgadmin/phppgadmin/releases/download/REL_7-13-0/phpPgAdmin-7.13.0.tar.gz
tar -xvf phpPgAdmin-7.13.0.tar.gz
mv phpPgAdmin-7.13.0 $PHPPGADMIN_HOME
cp -f $INSTALL_HOME/config.inc.php $PHPPGADMIN_HOME/conf/config.inc.php
DRUPAL_HOME=/var/www/html/drupal
cd $DRUPAL_HOME
git clone https://github.com/tripal/tripal_install.git
mv tripal_install/* ./
drush --include=. tripal-generic-install
systemctl restart httpd.service
