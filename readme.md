Requirements
============
* At least 1024MB RAM
* Clean CENTOS 7 64-bit installed

Tips
============
* You can use Oracle VirtualBox to run a virtual machine on your own system (https://www.virtualbox.org/)
* You can then install CENTOS 7 or even download an already installed version of CENTOS 7 via http://www.osboxes.org

WARNING: We cannot vouch for the osboxes.org virtual disk files but we used it during testing and it works.

Installation
============

MAJOR ASSUMPTION: YOU MUST HAVE A CLEAN CENTOS7 INSTALLED.
THIS SCRIPT USES DEFAULT FILES WHICH MAY OVERWRITE YOUR CUSTOMIZED FILES - DO NOT USE ON AN ALREADY USED CENTOS 7!

This will set up and install all necessary requirements for Tripal. Particularly:
* PHP 5.6 from REMI as well as additional PHP modules needed for Tripal and Drupal
* Drush 8.3.x
* Miscellaneous configuration files required to run Postgres, Tripal and Drupal

The rest is done with the official scripts bundled with Tripal V3
* Drupal
* Tripal
* Chado

Run the following commands in bash / command line as root:

yum install -y git

mkdir /tri

cd /tri

git clone https://gitlab.com/TreeGenes/tripal_rapid_installer_for_centos_7.git

cd tripal_rapid_installer_for_centos_7

chmod 0755 tripal_rapid_installer_centos7.sh

./tripal_rapid_installer_centos7.sh

Reference Tripal Documentation
==============================
https://tripal.readthedocs.io/en/latest/user_guide/install_tripal/rapid_install.html
