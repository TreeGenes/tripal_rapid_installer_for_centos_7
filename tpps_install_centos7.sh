DRUPAL_HOME=/var/www/html/drupal
cd $DRUPAL_HOME
rm -rf $DRUPAL_HOME/sites/all/modules/TGDR
rm -rf $DRUPAL_HOME/sites/all/modules/tripal_eutils
rm -rf $DRUPAL_HOME/sites/all/modules/tripal_manage_analyses
cd $DRUPAL_HOME/sites/all/modules
yum install git -y
git clone https://gitlab.com/TreeGenes/TGDR.git
drush dl ultimate_cron -y
drush en ultimate_cron -y
git clone https://github.com/NAL-i5K/tripal_eutils.git
git clone https://github.com/statonlab/tripal_manage_analyses.git
